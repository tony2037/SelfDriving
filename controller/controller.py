import argparse
import base64
from datetime import datetime
import os, sys
import shutil

import json

import numpy as np
import socketio
import eventlet
import eventlet.wsgi
from PIL import Image
import cv2
from flask import Flask
from io import BytesIO

from keras.models import model_from_json
sys.path.append('D:\\SelfDriving\\semantic-segmentation\\pipeline') # To extern the path
import evaluate



sio = socketio.Server()
app = Flask(__name__)
model = None
prev_image_array = None

MAX_SPEED = 25
MIN_SPEED = 10

speed_limit = MAX_SPEED

directory_path = ''

# transation: state_i, action_i, reward_i, state_i+1
buffer = [] # memory of every states




@sio.on('telemetry')
def telemetry(sid, data):
    if data:
        # The current steering angle of the car
        steering_angle = float(data["steering_angle"])
        # The current throttle of the car
        throttle = float(data["throttle"])
        # The current speed of the car
        speed = float(data["speed"])
        # The current image from the center camera of the car
        image = Image.open(BytesIO(base64.b64decode(data["image"])))
        # reward
        reward = float(data["reward"])

        # save frame
        #image.show()
        #os.system("pause")
        print('{} {} {}'.format(steering_angle, throttle, speed))
        # Save the data
        data={
            'steering_angle': steering_angle.__str__(),
            'throttle': throttle.__str__(),
            'speed': speed.__str__()
        }
        save_json_path = os.path.join(directory_path, '%s.json' % datetime.now().strftime('%S'))
        save_image_path = save_json_path.replace('json', 'png')
        save_predict_path = save_json_path.replace('.json', '_predict.png')
        with open(save_json_path, 'a') as outfile:
            json.dump(data, outfile)
            outfile.close()
        image.save(save_image_path, 'PNG')

        # Predict and make decision
        image = np.array(image, dtype=np.float32)
        predict = model.predict(np.expand_dims(image, axis = 0))
        for w in range(0, prev_image_array.shape[0]):
            for h in range(0, prev_image_array.shape[1]):
                prev_image_array[w][h] = evaluate.one_hot_to_BGR(predict[0][w][h])

        # Save the predict image
        cv2.imwrite(save_predict_path, prev_image_array)

        # Save the transation for learning
        buffer.append({"steering_angle": steering_angle, "throttle": throttle, "speed":speed,\
         "image": image.tostring, "action_angle": 0, "action_throttle": 0, "reward": reward})
        print(buffer)

        # Send the instruction to the server
        send_control(0.5, 0.5)
        
    else:
        # NOTE: DON'T EDIT THIS.
        sio.emit('manual', data={}, skip_sid=True)


@sio.on('connect')
def connect(sid, environ):
    print("connect ", sid)
    send_control(0, 0)


def send_control(steering_angle, throttle):
    sio.emit(
        "steer",
        data={
            'steering_angle': steering_angle.__str__(),
            'throttle': throttle.__str__()
        },
        skip_sid=True)


def create_directory():
    timestamp = datetime.now().strftime('%D-%H-%M').replace('/', '-')
    directory = os.path.join(os.getcwd(), 'data')
    directory = os.path.join(directory, '{%s}' % timestamp)
    if not os.path.exists(directory):
        os.mkdir(directory)
    return directory

def load_trained_model_with_FullModel(Model_json_path='./semantic-segmentation-model/model.json', Weights_h5_path='./semantic-segmentation-model/model.h5'):
    # The system configuration
    os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"   # see issue #152
    os.environ["CUDA_VISIBLE_DEVICES"]="1"
    # load json and create model
    json_file = open(Model_json_path , 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    model = model_from_json(loaded_model_json)
    # load weights into new model
    model.load_weights(Weights_h5_path)
    print("Loaded model from disk")

    return model


if __name__ == '__main__':
    directory_path = create_directory()

    model = load_trained_model_with_FullModel(Model_json_path='./semantic-segmentation-model/model.json', Weights_h5_path='./semantic-segmentation-model/model.h5')

    prev_image_array = np.zeros((model.layers[-1].output_shape[1], model.layers[-1].output_shape[2], 3))

    parser = argparse.ArgumentParser(description='Remote Driving')
    
    args = parser.parse_args()


    # wrap Flask application with engineio's middleware
    app = socketio.Middleware(sio, app)

    # deploy as an eventlet WSGI server
    eventlet.wsgi.server(eventlet.listen(('', 4567)), app)