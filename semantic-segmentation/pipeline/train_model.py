# Using framework : keras

from keras.models import Sequential, model_from_json
from keras.layers import Conv2D, MaxPooling2D, Dense, Flatten, UpSampling2D, Conv2DTranspose, BatchNormalization
from keras.callbacks import ModelCheckpoint, ReduceLROnPlateau, TensorBoard, EarlyStopping
import keras
import numpy as np
import cv2
from keras.preprocessing.image import ImageDataGenerator
from evaluate import load_trained_model_with_FullModel
from glob import glob

def load_data(total_number=916, x_path = "./dataset/128x256/features/", y_path = "./dataset/128x256/labels/"):

    x_train = []
    y_train = []
    x_test = []
    y_test = []

    for i in range(0, total_number):
        x = cv2.imread(x_path + str(i) + ".jpg")
        x = np.float32(x)
        x_train.append(x)

        y = np.load(y_path + str(i) + ".npy")
        y = np.float32(y)
        y_train.append(y)
    assert x_train[0].shape == (128,256,3)
    assert y_train[0].shape == (128,256,5)

    return (x_train, y_train), (x_test, y_test)

def ConstructDataGenerator():
    datagen = ImageDataGenerator(zca_whitening=False, rotation_range=40, width_shift_range=0.2, \
    height_shift_range=0.2, shear_range=0.2, zoom_range=0.2, horizontal_flip=True, fill_mode='nearest')
    return datagen

def Constructmodel(image_size):
    model = Sequential()
    model.add(Conv2D(32, (3, 3),strides=(1, 1), padding='same', input_shape=image_size, activation='relu'))
    model.add(BatchNormalization())
    model.add(Conv2D(32, (3, 3),strides=(1, 1), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Conv2D(32, (3, 3),strides=(1, 1), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Conv2D(32, (3, 3),strides=(1, 1), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2), padding='same'))
    model.add(Conv2D(64, (3, 3),strides=(1, 1), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Conv2D(64, (3, 3),strides=(1, 1), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Conv2D(64, (3, 3),strides=(1, 1), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Conv2D(64, (3, 3),strides=(1, 1), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2), padding='same'))
    model.add(Conv2D(128, (3, 3),strides=(1, 1), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Conv2D(128, (3, 3),strides=(1, 1), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Conv2D(128, (3, 3),strides=(1, 1), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Conv2D(128, (3, 3),strides=(1, 1), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Conv2D(128, (3, 3),strides=(1, 1), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Conv2D(128, (3, 3),strides=(1, 1), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2), padding='same'))
    model.add(Conv2D(256, (3, 3),strides=(1, 1), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Conv2D(256, (3, 3),strides=(1, 1), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Conv2D(256, (3, 3),strides=(1, 1), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2), padding='same'))
    model.add(Conv2D(256, (3, 3),strides=(1, 1), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Conv2D(256, (3, 3),strides=(1, 1), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Conv2D(256, (3, 3),strides=(1, 1), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2), padding='same'))
    #model.add(Flatten())
    #model.add(Dense(4096))
    #model.add(Dense(4096))
    model.add(Conv2D(2048, (4, 8),strides=(1, 1), padding='valid', activation='relu'))
    model.add(BatchNormalization())
    model.add(Conv2D(2048, (1, 1),strides=(1, 1), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Conv2DTranspose(256, (4,8), padding='valid', activation='relu'))
    model.add(BatchNormalization())
    model.add(UpSampling2D(size=(2, 2)))
    model.add(Conv2DTranspose(256, (3,3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Conv2DTranspose(256, (3,3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Conv2DTranspose(256, (3,3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(UpSampling2D(size=(2, 2)))
    model.add(Conv2DTranspose(256, (3,3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Conv2DTranspose(256, (3,3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Conv2DTranspose(128, (3,3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(UpSampling2D(size=(2, 2)))
    model.add(Conv2DTranspose(128, (3,3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Conv2DTranspose(128, (3,3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Conv2DTranspose(128, (3,3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Conv2DTranspose(128, (3,3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Conv2DTranspose(128, (3,3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Conv2DTranspose(64, (3,3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(UpSampling2D(size=(2, 2)))
    model.add(Conv2DTranspose(64, (3,3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Conv2DTranspose(64, (3,3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Conv2DTranspose(64, (3,3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Conv2DTranspose(32, (3,3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Conv2DTranspose(32, (3,3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Conv2DTranspose(32, (3,3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Conv2DTranspose(32, (3,3), padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(UpSampling2D(size=(2, 2)))
    model.add(Conv2D(5, (1, 1),strides=(1, 1), padding='same', activation='softmax'))
    model.summary()
    return model

def train(x_train, y_train, x_test, y_test, model_path = './model/HalfChannel_MoreLayers/', DataArgumentation = True, epochs = 5000):
    print(x_train[77].shape)
    print(y_train[77].shape)
    image_size = (128,256,3)
    
    if(glob('%smodel.h5' % model_path) != [] and glob('%smodel.json' % model_path) != []):
        print('Training with pretrained model')
        model = load_trained_model_with_FullModel(Model_json_path = '%smodel.json' % model_path, Weights_h5_path = '%smodel.h5' % model_path)
    else:
        print('There is no pretrained model')
        model = Constructmodel(image_size = image_size)
    model.compile(loss=keras.losses.categorical_crossentropy,
              optimizer=keras.optimizers.Adam(),
              metrics=['accuracy'])

    # keras callback function
    reduce_lr = ReduceLROnPlateau(monitor='val_loss', factor=0.2,
                              patience=5, min_lr=0.001)
    checkpointer = ModelCheckpoint(filepath='%sweights.h5' % model_path, monitor='val_loss', verbose=1, save_best_only=True, period=10, save_weights_only=True)
    tensorboad_log = TensorBoard(log_dir='%sGraph' % model_path, histogram_freq=0, write_graph=True, write_images=False, embeddings_freq=0, embeddings_layer_names=None, embeddings_metadata=None)
    early_stopping = EarlyStopping(monitor='val_loss', patience=20)

    # training
    if(DataArgumentation):
        datagen = ConstructDataGenerator()
        datagen.fit(x_train)
        model.fit_generator(datagen.flow(x_train, y_train, batch_size=8),
                    steps_per_epoch=len(x_train) / 8, epochs=epochs, callbacks=[reduce_lr,  tensorboad_log, checkpointer, early_stopping])
    else:
        train_history = model.fit(x=x_train,  
                            y=y_train, validation_split=0.2,  
                            epochs=epochs, batch_size=8, verbose=1, callbacks=[reduce_lr,  tensorboad_log, checkpointer, early_stopping])
    # serialize model to JSON
    model_json = model.to_json()
    with open('%smodel.json' % model_path, "w") as json_file:
        json_file.write(model_json)
    # serialize weights to HDF5
    model.save_weights('%smodel.h5' % model_path)
    print("Saved model to disk")
    #Deconvolution2D(3, 3, 3, output_shape=(None, 3, 14, 14),border_mode='valid',input_shape=(3, 12, 12))
    #model.add(UpSampling2D(size=(2, 2),input_shape=image_size))

if __name__=="__main__":

    x_train = []
    y_train = []
    x_test = []
    y_test = []
    (x_train, y_train), (x_test, y_test) = load_data(880, x_path='./dataset/128x256/sim_features/', y_path='./dataset/128x256/sim_labels/')
    x_train = np.array(x_train, dtype=np.float32)
    y_train = np.array(y_train, dtype=np.float32)
    x_test = np.array(x_test, dtype=np.float32)
    y_test = np.array(y_test, dtype=np.float32)
    train(x_train, y_train, x_test, y_test, model_path = './model/FineTune_simulator/', DataArgumentation = False, epochs = 2000)
