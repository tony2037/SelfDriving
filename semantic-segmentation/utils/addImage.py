import cv2
from glob import glob

def inputFlow(path = './dataset/128x256/evaluate_features/'):
    glob_features = glob('%s*.png' % path)
    glob_labels = []
    evaluate_features = []
    evaluate_labels = []

    names = []
    for i in glob_features:
        i = i.split('\\')
        names.append(i[-1])
    
    for i in names:
        glob_labels.append('%sevaluate_labels/predict_%s' %(path, i))

    for i in glob_features:
        evaluate_features.append(cv2.imread(i))

    for i in glob_labels:
        evaluate_labels.append(cv2.imread(i))

    return evaluate_features, evaluate_labels, names

def addImage(evaluate_features, evaluate_labels, save_path = './results/', names = None):
    if(names is None):
        exit('Cannot get the names list')
    for i in range(0, len(evaluate_features)):
        file_name = '%s%s' % (save_path, names[i])
        print(file_name)
        img = cv2.addWeighted(evaluate_features[i], 0.6, evaluate_labels[i], 0.4, 0)
        cv2.imwrite(file_name, img)

if __name__ == '__main__':
    evaluate_features, evaluate_labels, names = inputFlow(path = './dataset/128x256/sim_features/')
    addImage(evaluate_features = evaluate_features, evaluate_labels = evaluate_labels, save_path = './dataset/128x256/sim_results/', names = names)