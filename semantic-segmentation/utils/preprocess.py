#!/usr/bin/env python3
# Working under path : ......./semantic-segmentation/python3 ./utils/preprocess.py
from glob import glob
import cv2
import numpy as np
from argparse import ArgumentParser
import json

def resize(height, width, image_path, save_path, image_number):
    image = cv2.imread(image_path).astype(np.float32)
    image = cv2.resize(image, (height, width), interpolation=cv2.INTER_CUBIC)
    cv2.imwrite(save_path+ str(image_number)+ ".png", image)
    return image

def category(BGR):
    pixel2label = [
        [142, 0, 0],     # car
        [128, 64, 128],  # road
        [180, 130, 70],   # sky
        [160, 170, 250], # parking
    ]

    vec = [0] * 5
    BGR = BGR.tolist()

    if BGR in pixel2label:
        vec[pixel2label.index(BGR)] = 1
    else:
        vec[4] = 1

    if(vec == [0, 0, 0, 1, 0]):
        print("Parking label occured")
    return vec

def onehot(image, save_path, label_number):
    encoded = np.zeros(( image.shape[0], image.shape[1], 5), dtype = np.float32)
    for i in range(0, image.shape[0]):
        for j in range(0, image.shape[1]):
            encoded[i][j] = category(image[i][j])
    
    encoded = np.float32(encoded)
    np.save(save_path + str(label_number) + ".npy", encoded)
    return encoded

def area_list(path = './dataset/128x256/area.txt'):
    fp = open(path)
    areas = ""
    areas = fp.readline()
    areas = areas.split(' ')
    return areas

def interface(image_path = './dataset/leftImg8bit/train/', label_path = './dataset/gtCoarse/train/', image_suffix = 'leftImg8bit.png', \
label_suffix = 'gtCoarse_color.png', features_save_path = './dataset/128x256/features/', labels_save_path = './dataset/128x256/labels/', \
area = './dataset/128x256/area.txt', height = 256, width = 128):
    # Variables 
    AreaList = area_list(path = area)
    current_data_number = 0
    # Dealing with the file list
    for area in AreaList:
        images = []
        labels = []
        images = glob('%s%s/*.png' % (image_path , area))
        if(len(images) == 0):
            break
        print('%s%s/*.png' % (image_path , area))
        # dealing with labels name
        for i in images:
            i = i.split('\\')
            i = i[-1]
            i = '%s%s/%s%s' % (label_path, area, i[:-(len(image_suffix))], label_suffix)
            labels.append(i)
        # Transform data
        for image, label in zip(images, labels):
            current_data_number = current_data_number + 1
            image = resize(height = height, width = width, image_path = image, save_path = features_save_path, image_number = current_data_number)
            label = resize(height = height, width = width, image_path = label, save_path = labels_save_path, image_number = current_data_number)
            label = onehot(label, save_path = labels_save_path, label_number = current_data_number)

def ArgumentParserConstructor():
    parser = ArgumentParser()
    parser.add_argument('--mode', metavar='N', type=str, nargs='+',
                    help='The mode of preprocess', default = 'default')
    parser.add_argument('--path', "--optional-arg", help="The path of config.json", dest="path", default="./config.json")
    args = parser.parse_args()
    return args

if __name__ == '__main__':
    print('Please provide the information below : ')
    print('config.json')
    args = ArgumentParserConstructor()
    mode = args.mode[0]
    print('position arg:', mode)
    print("optional arg:", (args.path))
    if(mode == 'default'):
        interface()
    elif(mode == 'json' or mode == 'j'):
        data = None
        with open(args.path) as f:
            data = json.load(f)
        print(data)
        interface(image_path = data['image_path'], label_path = data['label_path'], image_suffix = data['image_suffix'], \
        label_suffix = data['label_suffix'], features_save_path = data['features_save_path'], labels_save_path = data['labels_save_path'], \
        area = data['area_list'], height = data['height'], width = data['width'])
    else:
        print("No such mode %s" %args.mode)
        exit()
    

        
"""
    image_path = "./dataset/leftImg8bit/train/zurich"
    label_path = "./dataset/gtFine/train/zurich"
    suffix = "_gtFine_color.png"
    features_save_path = "./dataset/dataset224x224/features/"
    labels_save_path = "./dataset/dataset224x224/labels/"
    current_data_number = int(open("./dataset/dataset224x224/features/log", "r").read())

    images = glob('%s/*.png' % image_path)
    labels = []
    #print(images[0][35:-16])

    for i in images:
        print('%s/%s%s' % (label_path, i[35:-16], suffix))
        labels.append('%s/%s%s' % (label_path, i[35:-16], suffix))

    for image_path, label_path in zip(images, labels):
        current_data_number = current_data_number + 1
        image = resize(height = 224, width = 224, image_path = image_path, save_path = features_save_path, image_number = current_data_number)
        label = resize(height = 224, width = 224, image_path = label_path, save_path = labels_save_path, image_number = current_data_number)
        label = onehot(label, save_path = labels_save_path, label_number = current_data_number)
# save log
open("./dataset/dataset224x224/features/log", "w").write(str(current_data_number))
"""