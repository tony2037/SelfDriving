import sys, os
sys.path.append('D:\\SelfDriving\\semantic-segmentation\\pipeline') # To extern the path
import evaluate
import cv2
from glob import glob
import numpy as np

fps = 20
width = 256
height = 128
size = (width, height)
video_path = './dataset/leftImg8bit_demoVideo/leftImg8bit/demoVideo/stuttgart_00/'
idex_name = video_path.split('/')[-2]
videoWriter = cv2.VideoWriter('%s%s.avi' % (video_path, idex_name), cv2.VideoWriter_fourcc(*'DIVX'), fps, size)
#videoWriter = cv2.VideoWriter('%s%s.avi' % (video_path, idex_name), -1, 1, size)
videoWriter_predict = cv2.VideoWriter('%s%s_predict.avi' % (video_path, idex_name), cv2.VideoWriter_fourcc(*'DIVX'), fps, size)
#videoWriter_predict = cv2.VideoWriter('%s%s_predict.avi' % (video_path, idex_name), -1, 1, size)
# reference : https://stackoverflow.com/questions/14440400/creating-a-video-using-opencv-2-4-0-in-python

def resize(height = 128, width = 256, image_path = ''):
    image = cv2.imread(image_path).astype(np.float32)
    image = cv2.resize(image, (width, height), interpolation=cv2.INTER_CUBIC)
    return image

def BuildUp(Model_json_path = './model/HalfChannel_MoreLayers/model.json', Weights_h5_path = './model/HalfChannel_MoreLayers/weights.h5'):
    os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"   # see issue #152
    os.environ["CUDA_VISIBLE_DEVICES"]="1"
    model = evaluate.load_trained_model_with_FullModel(Model_json_path = Model_json_path, Weights_h5_path = Weights_h5_path)
    return model

def streamWithPics(image_path = video_path, model = None, addImage = False):
    images = glob('%s*.png' % image_path)
    for i in images:
        print(i.split('\\')[-1])
    # u have to make sure the images are correct sequentialy
    for i in images:
        print('dealing with %s' % i.split('\\')[-1])
        image = resize(width = width, height = height, image_path = i)
        videoWriter.write(np.uint8(image))
        predict = model.predict(np.expand_dims(image, axis = 0))
        temp = np.zeros((predict.shape[1], predict.shape[2], 3))
        for w in range(0, temp.shape[0]):
            for h in range(0, temp.shape[1]):
                temp[w][h] = evaluate.one_hot_to_BGR(predict[0][w][h])

        if(addImage):
            temp = cv2.addWeighted(np.uint8(image), 0.6, np.uint8(temp), 0.4, 0)
            videoWriter_predict.write(temp)
        else:
            videoWriter_predict.write(np.uint8(temp))
        

def streamWithVideo(video_path = ''):
    pass

if __name__ == '__main__':
    model = BuildUp(Model_json_path = './model/DataArgumentation/model.json', Weights_h5_path = './model/DataArgumentation/weights.h5')
    streamWithPics(model = model, addImage = True)
    